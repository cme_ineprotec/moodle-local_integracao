<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . "/externallib.php");

/**
 * Class local_wsintegracao_external
 *
 * @package    local_integracao
 * @copyright  2019 Willian Mano {@link http://conecti.me}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class local_integracao_external extends external_api {
    /**
     * @return external_function_parameters
     */
    public static function create_user_parameters() {
        return new external_function_parameters(
            [
                'user' => new external_single_structure([
                    'courseid' => new external_value(PARAM_INT, 'Id do curso para matricular o aluno'),
                    'firstname' => new external_value(PARAM_TEXT, 'Primeiro nome do user'),
                    'lastname' => new external_value(PARAM_TEXT, 'Ultimo nome do user'),
                    'email' => new external_value(PARAM_TEXT, 'Email do user'),
                    'username' => new external_value(PARAM_TEXT, 'Usuario de acesso do user'),
                    'password' => new external_value(PARAM_TEXT, 'Senha do user'),
                    'city' => new external_value(PARAM_TEXT, 'Cidade do user')
                ])
            ]
        );
    }

    /**
     * @param $user
     *
     * @return null
     *
     * @throws dml_transaction_exception
     * @throws invalid_parameter_exception
     * @throws dml_exception
     */
    public static function create_user($user) {
        global $DB;

        // Validação dos paramêtros.
        self::validate_parameters(self::create_user_parameters(), ['user' => $user]);

        $user = (object)$user;

        $course = $DB->get_record('course', ['id' => $user->courseid]);

        if (!$course) {
            return [
                'id' => 0,
                'status' => 'error',
                'message' => 'Não existe um curso com o ID informado'
            ];
        }

        $returndata = null;
        try {
            // Inicia a transacao, qualquer erro que aconteca o rollback sera executado.
            $transaction = $DB->start_delegated_transaction();

            // Verifica se o aluno ja existe na base, caso contrario cria a conta do usuario.
            $sql = "SELECT id FROM {user} WHERE email = :email OR username = :username";

            $params = ['email' => $user->email, 'username' => $user->username];

            $moodleuser = $DB->get_record_sql($sql, $params);

            if ($moodleuser) {
                $userid = $moodleuser->id;
            } else {
                // Cria a conta do usuário.
                $userid = self::moodle_create_user($user);
            }

            // Matricula o aluno em um curso no moodle.
            $userrole = 5;
            self::moodle_enrol_user($userid, $user->courseid, $userrole);

            $returndata = [
                'id' => $userid,
                'status' => 'success',
                'message' => 'Aluno matriculado com sucesso'
            ];

            // Persiste as operacoes em caso de sucesso.
            $transaction->allow_commit();
        } catch (\Exception $e) {
            $transaction->rollback($e);

            $returndata = [
                'id' => 0,
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }

        return $returndata;
    }

    /**
     * @return external_single_structure
     */
    public static function create_user_returns() {
        return new external_single_structure(
            array(
                'id' => new external_value(PARAM_INT, 'Id'),
                'status' => new external_value(PARAM_TEXT, 'Status da operacao'),
                'message' => new external_value(PARAM_TEXT, 'Mensagem de retorno da operacao')
            )
        );
    }

    /**
     * @param $user
     * @return int
     *
     * @throws dml_exception
     * @throws moodle_exception
     */
    protected static function moodle_create_user($user) {
        global $CFG;

        // Inclui a biblioteca de usuarios do moodle.
        require_once("{$CFG->dirroot}/user/lib.php");

        // Cria o usuario usando a biblioteca do proprio moodle.
        $user->confirmed = 1;
        $user->mnethostid = 1;

        return user_create_user($user);
    }

    /**
     * @param $userid
     * @param $courseid
     * @param $roleid
     *
     * @throws coding_exception
     * @throws dml_exception
     */
    protected static function moodle_enrol_user($userid, $courseid, $roleid) {
        global $CFG;

        $courseenrol = self::get_course_enrol($courseid);

        require_once($CFG->libdir . "/enrollib.php");
        if (!$enrolmanual = enrol_get_plugin('manual')) {
            throw new coding_exception('Can not instantiate enrol_manual');
        }

        $enrolmanual->enrol_user($courseenrol, $userid, $roleid, time());
    }

    /**
     * @param $courseid
     *
     * @return mixed
     *
     * @throws dml_exception
     */
    protected static function get_course_enrol($courseid) {
        global $DB;

        return $DB->get_record('enrol', ['courseid' => $courseid, 'enrol' => 'manual'], '*', MUST_EXIST);
    }
}