<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/*
 * Web Service local plugin functions and services definition
 *
 * @package    local_integracao
 * @copyright  2019 Willian Mano {@link http://conecti.me}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$functions = array(
    'local_integracao_create_user' => array(
        'classname' => 'local_integracao_external',
        'classpath'   => 'local/integracao/classes/external.php',
        'methodname' => 'create_user',
        'description' => 'Creates an user and enrol him/her on a course.',
        'type' => 'write',
        'services' => array(MOODLE_OFFICIAL_MOBILE_SERVICE)
    )
);

$services = array(
    'Integracao' => array(
        'functions' => array(
            'local_integracao_create_user'
        ),
        'restrictedusers' => 0,
        'enabled' => 1
    )
);
